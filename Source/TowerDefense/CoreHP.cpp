// Fill out your copyright notice in the Description page of Project Settings.


#include "CoreHP.h"
#include "PlayerCore.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ACoreHP::ACoreHP()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACoreHP::BeginPlay()
{
	Super::BeginPlay();

	health = 100;

	UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlayerCore::StaticClass(), Cores);
	for (int i = 0; i < Cores.Num(); i++)
	{
		AttachToActor(this, FAttachmentTransformRules(EAttachmentRule::KeepWorld, true), ACoreHP::GetAttachParentSocketName());
		APlayerCore* core = Cast<APlayerCore>(Cores[i]);
		//core->CoreHit.AddDynamic(this, &ACoreHP::damageCore);
	}
}

void ACoreHP::damageCore(APlayerCore* Core)
{
	health -= 2;

}

// Called every frame
void ACoreHP::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}