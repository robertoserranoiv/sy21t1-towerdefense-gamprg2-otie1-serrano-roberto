// Fill out your copyright notice in the Description page of Project Settings.


#include "Spawner.h"
#include "TowerDefenseGameModeBase.h"
#include "EnemyAI.h"
#include "EnemyAIController.h"
#include "WaveData.h"
#include "Kismet/GameplayStatics.h"	
#include "TimerManager.h"
#include "Components/ArrowComponent.h"
#include "HealthComponent.h"

// Sets default values
ASpawner::ASpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	StaticMesh->SetupAttachment(RootComponent);

	arrow = CreateDefaultSubobject<UArrowComponent>(TEXT("ArrowComponent"));
	arrow->SetupAttachment(StaticMesh);
}
// Called when the game starts or when spawned
void ASpawner::BeginPlay()
{
	Super::BeginPlay();

	TowerGameMode = Cast<ATowerDefenseGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()->GetAuthGameMode()));

	NumOfEnemiesToSpawn = WaveData[currentWave]->GetNumOfSpawn();

	if (WaveData.Num() > 0)
	{
		SpawnEnemy();
	}

}

// Called every frame
void ASpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASpawner::SpawnEnemy()
{
	NextWaveDelay.Invalidate();

	if (NumOfEnemiesToSpawn > 0)
	{
		if (WaveData[currentWave]->Enemies > 0)
		{
			UWorld* world = GetWorld();
			FActorSpawnParameters spawnParameters;
			spawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			AEnemyAI* enemy = world->SpawnActor<AEnemyAI>(WaveData[currentWave]->Enemies, GetActorTransform(), spawnParameters);	

			enemy->healthComponent->InitializeHealth(WaveData[currentWave]->Multiplier);

			enemy->GoldReward += WaveData[currentWave]->KillReward * WaveData[currentWave]->Multiplier;
			enemy->OnEnemyDeath.AddDynamic(this, &ASpawner::OnEnemyKilled);

			enemy->CurrentWaypoint = this->CurrentWaypoint;
			enemy->FinalWaypoint = this->FinalWaypoint;
			enemy->MovetoWaypoints();
			NumOfEnemiesToSpawn--;

			FTimerDelegate TimerDelegate = FTimerDelegate::CreateUObject(this, &ASpawner::SpawnEnemy);
			GetWorldTimerManager().SetTimer(SpawnDelay, TimerDelegate, 2.0f, false);
		}
	}

	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Stopped Spawning"));
		SpawnDelay.Invalidate();

		if (currentWave < WaveData.Num() - 1)
		{
			endWave.Broadcast(this);
		}
	}
}

void ASpawner::StartNextWave()
{
	GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Red, "Wave Clear");
	TowerGameMode->AddGoldWave(WaveData[currentWave]->ClearReward);

	if (currentWave < WaveData.Num() - 1)
	{
		GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Red, "Next Wave");
		currentWave++;
		NumOfEnemiesToSpawn = WaveData[currentWave]->GetNumOfSpawn();

		FTimerDelegate TimerDelegate = FTimerDelegate::CreateUObject(this, &ASpawner::SpawnEnemy);
		GetWorldTimerManager().SetTimer(NextWaveDelay, TimerDelegate, WaveData[currentWave]->Duration, false);
	}

	else
	{
		return;
	}
}

int32 ASpawner::GetCurrentWave()
{
	return currentWave;
}

void ASpawner::AddCurrentWave()
{
	currentWave++;
}

void ASpawner::OnEnemyKilled(AEnemyAI* enemy)
{
	TowerGameMode->AddGold(enemy);
}
