// Fill out your copyright notice in the Description page of Project Settings.


#include "TowerNode.h"
#include "Tower.h"
#include "BuildManager.h"
#include "Components/StaticMeshComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/PrimitiveComponent.h"
#include "GameFramework/Character.h"


// Sets default values
ATowerNode::ATowerNode()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	Arrow = CreateDefaultSubobject<UArrowComponent>(TEXT("Arrow"));
	StaticMesh->SetupAttachment(RootComponent);
	Arrow->SetupAttachment(StaticMesh);
}

// Called when the game starts or when spawned
void ATowerNode::BeginPlay()
{
	Super::BeginPlay();

	IsOccupied = false;
	StaticMesh->OnClicked.AddDynamic(this, &ATowerNode::OnClickedEvent);
}

/*void ATowerNode::BuildTower(TSubclassOf<class ATower> tower)
{
	ATower* towers = GetWorld()->SpawnActor<ATower>(tower, StaticMesh->GetComponentLocation(), StaticMesh->GetComponentRotation());
	IsOccupied = true;
}*/

void ATowerNode::OnClickedEvent(UPrimitiveComponent* TouchedComponent, FKey ButtonPressed)
{
	if (BuildManager)
	{
		if (BuildManager->IsBuilding())
		{
			if (!IsOccupied)
			{
				//BuildManager->BuildTower(GetArrowLocation(), GetArrowRotation());
				BuildManager->BuildTower(GetActorLocation(), GetActorRotation());
				IsOccupied = true;
			}
		}
		else
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("not in build node"));
		}
	}
}

// Called every frame
void ATowerNode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

