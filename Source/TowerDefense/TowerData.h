// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "TowerData.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class TOWERDEFENSE_API UTowerData : public UDataAsset
{
	GENERATED_BODY()
	
public:

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 GoldValue;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TSubclassOf<class ATowerGhost> TowerGhost;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TSubclassOf<class ATower> Tower;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		FVector RangeScale;
};
