// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCore.h"
#include "Components/StaticMeshComponent.h"
#include "EnemyAI.h"
#include "TowerDefenseGameModeBase.h"

// Sets default values
APlayerCore::APlayerCore()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	StaticMesh->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void APlayerCore::BeginPlay()
{
	Super::BeginPlay();
	
	StaticMesh->OnComponentHit.AddDynamic(this, &APlayerCore::OnHit);
}

void APlayerCore::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (AEnemyAI* EnemyAI = Cast<AEnemyAI>(OtherActor))
	{
		OnTrigger(EnemyAI);
		CoreHit.Broadcast(this);
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Core Hit"));
	}
}

// Called every frame
void APlayerCore::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
