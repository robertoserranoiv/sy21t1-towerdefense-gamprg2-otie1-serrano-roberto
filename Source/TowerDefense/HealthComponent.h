// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDeath);

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TOWERDEFENSE_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UHealthComponent();

	FOnDeath enemyDead;
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float CurrentHP;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float MaxHP = 100.0f;

	//AActor* Owner;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class ATowerDefenseGameModeBase* TowerGameMode;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class AEnemyAI* EnemyAI;

	UFUNCTION(BlueprintCallable)
		void TakeDamage(int32 damage);

	UFUNCTION()
		void InitializeHealth(int32 healthMultiplier);

	UFUNCTION()
		float GetCurrentHP();
};
