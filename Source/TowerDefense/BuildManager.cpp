// Fill out your copyright notice in the Description page of Project Settings.


#include "BuildManager.h"
#include "TowerNode.h"
#include "TowerGhost.h"
#include "Tower.h"
#include "TowerData.h"
#include "TowerDefenseGameModeBase.h"
#include "Kismet/GameplayStatics.h"	

// Sets default values
ABuildManager::ABuildManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABuildManager::BeginPlay()
{
	Super::BeginPlay();
	
	PlayerController = Cast<APlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	PlayerController->bShowMouseCursor = true;
	PlayerController->bEnableClickEvents = true;
}

// Called every frame
void ABuildManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (GhostTower)
	{
		FHitResult Hit;
		PlayerController->GetHitResultUnderCursorByChannel(TraceTypeQuery1, true, Hit);

		if (ATowerNode* TowerNode = Cast<ATowerNode>(Hit.GetActor()))
		{
			FVector Location = TowerNode->GetActorLocation();
			//FVector Location = TowerNode->GetArrowLocation();
			Location.Z -= 55.0f;
			//GhostTower->SetActorLocationAndRotation(Location, TowerNode->GetArrowRotation());
			GhostTower->SetActorLocationAndRotation(TowerNode->GetActorLocation(), TowerNode->GetActorRotation());
		}

		else

		{
			if (GhostTower)
			{
				GhostTower->SetActorLocation(Hit.Location);
			}
		}
	}

}

void ABuildManager::BuildModeStart(int32 index)
{
	if (UWorld* World = GetWorld())
	{
		if (PlayerController)
		{
			IsInBuildNode = true;
			FActorSpawnParameters SpawnParams;
			SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
			FRotator Rotation = FRotator::ZeroRotator;
			FVector Location = FVector::ZeroVector;

			FVector RangeScale = TowerData[index]->RangeScale;
			GhostTower = World->SpawnActor<ATowerGhost>(GhostArray[index], Location, Rotation, SpawnParams);
			GhostTower->SetRangePreview(RangeScale);
		}
	}
	TowerIndex = index;
}

void ABuildManager::BuildModeEnd()
{
	GhostTower->Destroy();
	IsInBuildNode = false;
	TowerIndex = NULL;
}

bool ABuildManager::IsPlayerGoldEnough(float cost)
{
	if (GameMode->PlayerGold > cost)
	{
		return true;
	}
	else
	{
		return false;
	}
	
}

void ABuildManager::DeductGold(float cost)
{
	//int32 GoldValue = cost * -1;
	GameMode->PlayerGold -= cost;
	//GameMode->AddGold(GoldValue);
}


void ABuildManager::BuildTower(FVector Location, FRotator Rotator)
{
	if (UWorld* World = GetWorld())
	{
		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
		Location.Z -= 55.0f;
		ATower* Tower = World->SpawnActor<ATower>(TowerArray[TowerIndex], Location, Rotator, SpawnParams);

		int32 GoldValue = TowerData[TowerIndex]->GoldValue * -1;
		//GameMode->AddGold(GoldValue);
		BuildModeEnd();
	}
}


