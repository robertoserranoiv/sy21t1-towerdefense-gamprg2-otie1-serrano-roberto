// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BuildManager.generated.h"

UCLASS()
class TOWERDEFENSE_API ABuildManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABuildManager();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

		APlayerController* PlayerController;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Components")
		TArray<class UTowerData*> TowerData;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
		TArray<TSubclassOf<class ATowerGhost>> GhostArray;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
		TArray<TSubclassOf<class ATower>> TowerArray;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
		bool IsInBuildNode;

	int32 TowerIndex;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class ATowerGhost* GhostTower;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class ATowerDefenseGameModeBase* GameMode;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
		FORCEINLINE bool IsBuilding() { return IsInBuildNode; }

	UFUNCTION(BlueprintCallable)
		void BuildModeStart(int32 index);

	UFUNCTION(BlueprintCallable)
		void BuildModeEnd();

	UFUNCTION()
		void BuildTower(FVector Location, FRotator Rotator);

	UFUNCTION(BlueprintCallable)
		bool IsPlayerGoldEnough(float cost);

	UFUNCTION(BlueprintCallable)
		void DeductGold(float cost);
};
