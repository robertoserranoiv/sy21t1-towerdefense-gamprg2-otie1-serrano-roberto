// Fill out your copyright notice in the Description page of Project Settings.


#include "Projectile.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "EnemyAI.h"
#include "HealthComponent.h"
#include "TowerDefenseGameModeBase.h"

// Sets default values
AProjectile::AProjectile()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;

    RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
    Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
    Collider = CreateDefaultSubobject<USphereComponent>(TEXT("Collider"));
    ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Projectile Movement"));

    Mesh->SetupAttachment(RootComponent);
    Collider->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AProjectile::BeginPlay()
{
    Super::BeginPlay();
    
    Collider->OnComponentBeginOverlap.AddDynamic(this, &AProjectile::OnBeginOverlap);
}

void AProjectile::OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
    if (AEnemyAI* enemy = Cast<AEnemyAI>(OtherActor))
    {
        enemy->healthComponent->TakeDamage(ProjectileDamage);
        this->Destroy();
    }

    /*else
    {
        this->Destroy();
    }*/
}

// Called every frame
void AProjectile::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

}

void AProjectile::SetDamage(int32 damage)
{
    ProjectileDamage = damage;
}

