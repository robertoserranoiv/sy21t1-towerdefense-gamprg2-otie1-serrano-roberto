// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Spawner.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWaveEnded, class ASpawner*, spawner);

UCLASS()
class TOWERDEFENSE_API ASpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawner();
		
	UPROPERTY(BlueprintAssignable)
		FOnWaveEnded endWave;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class UStaticMeshComponent* StaticMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UArrowComponent* arrow;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 NumOfEnemiesToSpawn;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 currentWave = 0;

		FTimerHandle SpawnDelay;

		FTimerHandle NextWaveDelay;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<AActor*> Waypoints;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 CurrentWaypoint;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 FinalWaypoint;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class ATowerDefenseGameModeBase* TowerGameMode;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<class UWaveData*> WaveData;

	UFUNCTION()
		void StartNextWave();

	UFUNCTION()
		void SpawnEnemy();

	UFUNCTION()
		int32 GetCurrentWave();

	UFUNCTION()
		void AddCurrentWave();

	UFUNCTION()
		void OnEnemyKilled(AEnemyAI* enemy);
};
