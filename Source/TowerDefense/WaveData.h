// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "WaveData.generated.h"

/**
 * 
 */	
UCLASS(BlueprintType)
class TOWERDEFENSE_API UWaveData : public UDataAsset
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float Duration;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float delayDuration;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 NumSpawns;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)	
		TSubclassOf<class AEnemyAI> Enemies;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Reward")
		int32 KillReward;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Reward")
		int32 ClearReward;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Health")
		int32 Multiplier;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Gold")
		int32 GoldMultiplier;

	UFUNCTION(BlueprintPure, BlueprintCallable)
		int32 TotalKillReward();

	UFUNCTION()
		int32 GetNumOfSpawn();

	UFUNCTION()
		float GetDuration();
};
