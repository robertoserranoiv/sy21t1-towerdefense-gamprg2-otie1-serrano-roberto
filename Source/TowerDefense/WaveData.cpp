// Fill out your copyright notice in the Description page of Project Settings.


#include "WaveData.h"

int32 UWaveData::TotalKillReward()
{
	return KillReward * NumSpawns;
}

int32 UWaveData::GetNumOfSpawn()
{
	return NumSpawns;
}

float UWaveData::GetDuration()
{
	return Duration;
}
