// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "EnemyAI.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnEnemyDied, class AEnemyAI*, EnemyAI);

UCLASS()
class TOWERDEFENSE_API AEnemyAI : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemyAI();

	UPROPERTY(BlueprintAssignable)
		FOnEnemyDied OnEnemyDeath;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
		void Die();

	bool IsDead;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 GoldReward;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 CurrentWaypoint;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "true"))
		int32 FinalWaypoint;

	void MovetoWaypoints();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UHealthComponent* healthComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class ATowerDefenseGameModeBase* TowerGameMode;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<class UWaveData*> WaveData;

private:

	TArray<AActor*> Waypoints;
};