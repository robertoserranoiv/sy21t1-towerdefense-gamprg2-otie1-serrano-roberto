// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TowerGhost.generated.h"

UCLASS()
class TOWERDEFENSE_API ATowerGhost : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATowerGhost();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UStaticMeshComponent* TowerBody;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UStaticMeshComponent* TowerBarrel;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class USphereComponent* Sphere;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UArrowComponent* Arrow;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool IsSpawn;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class APlayerController* PlayerController;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class ATowerDefenseGameModeBase* GameMode;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class ATower* Tower;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 DataIndex;

	UPROPERTY(EditAnywhere)
		class UMaterial* GreenMaterial;

	UPROPERTY(EditAnywhere)
		class UMaterial* RedMaterial;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class UTowerData* towerData;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
		void SetRangePreview(FVector RangeScale);

	UFUNCTION()
		void GhostToMouse();

	UFUNCTION()
		void UpdateGhostMaterial();

	UFUNCTION()
		void SpawnTower();

	UFUNCTION()
		void OnClicked(UPrimitiveComponent* TouchedComponent, FKey ButtonPressed);

private:

	const FTransform spawnLocation;

	const FRotator spawnRotation;

};
