// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Tower.generated.h"

UCLASS()
class TOWERDEFENSE_API ATower : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATower();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UStaticMeshComponent* TowerBody;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UStaticMeshComponent* TowerBarrel;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UArrowComponent* Arrow;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class USphereComponent* SphereCollider;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		TArray<class AEnemyAI*> Targets;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class AEnemyAI* CurrentTarget;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TSubclassOf<class AProjectile> Projectile;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float FireRate;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float FireTime;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		FVector TowerOffset;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 TowerDamage;

	UFUNCTION()
		void OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void OnEndOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
		void Aim();


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
		void ShootTarget();
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool IsActive;
};
