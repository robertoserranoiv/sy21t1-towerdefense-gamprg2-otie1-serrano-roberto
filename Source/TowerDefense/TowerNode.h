// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TowerNode.generated.h"

UCLASS()
class TOWERDEFENSE_API ATowerNode : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATowerNode();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UStaticMeshComponent* StaticMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class ABuildManager* BuildManager;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UArrowComponent* Arrow;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class ATower* Tower;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool IsOccupied;

	//UFUNCTION()
		//void BuildTower(TSubclassOf<class ATower> tower);

	UFUNCTION()
		void OnClickedEvent(UPrimitiveComponent* TouchedComponent, FKey ButtonPressed);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
