	// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TowerDefenseGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ATowerDefenseGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
protected:
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
		void OnWaveEnded(ASpawner* spawner);


public:
	//Core Properties
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int32 CoreMaxHp = 100;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 PlayerGold = 100;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class AEnemyAI* EnemyAI;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		TArray<AActor*> CoreActors;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<class UWaveData*> WaveData;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<class UTowerData*> TowerData;
		
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int32 CoreCurrentHp;

	//Wave Properties
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		TArray<AActor*> Spawners;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int32 totalEnemiesKilled;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int32 enemiesKilled;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int32 currentWave;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int32 currentWaveUI;

		FTimerHandle waveEndDelay;

	//Core Function
	UFUNCTION()
		void CoreTakeDamage(int damage);

	UFUNCTION()
		void CoreOnHit(class APlayerCore* coreActor);

	UFUNCTION()
		void CoreDestroyed();

	UFUNCTION()
		void RestartGame();

	UFUNCTION(BlueprintCallable)
		FORCEINLINE int32 GetGold() { return PlayerGold;  }

	UFUNCTION()
		int32 AddGold(AEnemyAI* enemy);

	UFUNCTION()
		int32 AddGoldWave(int32 amount);

	UFUNCTION()
		void SetGoldScale(int32 GoldMultiplier);

	UFUNCTION(BlueprintCallable)
		bool IsPlayerGoldEnough(float cost);

	UFUNCTION(BlueprintCallable)
		void DeductGold(float cost);
};
