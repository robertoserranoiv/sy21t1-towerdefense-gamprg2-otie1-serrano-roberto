// Fill out your copyright notice in the Description page of Project Settings.

#include "EnemyAI.h"
#include "EnemyAIController.h"
#include "Kismet/GameplayStatics.h"
#include "Waypoint.h"
#include "HealthComponent.h"
#include "WaveData.h"
#include "TowerDefenseGameModeBase.h"

// Sets default values
AEnemyAI::AEnemyAI()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	healthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("Health Component"));	
}

// Called when the game starts or when spawned
void AEnemyAI::BeginPlay()
{
	Super::BeginPlay();
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AWaypoint::StaticClass(), Waypoints);
	MovetoWaypoints();
	
	healthComponent->enemyDead.AddDynamic(this, &AEnemyAI::Die);
}

// Called every frame
void AEnemyAI::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AEnemyAI::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AEnemyAI::MovetoWaypoints()
{
	AEnemyAIController* EnemyAIController = Cast<AEnemyAIController>(GetController());

	if (EnemyAIController)
	{
		if (CurrentWaypoint <= Waypoints.Num() && CurrentWaypoint != FinalWaypoint + 1)
		{
			for (AActor* Waypoint : Waypoints)
			{
				AWaypoint* WaypointItr = Cast<AWaypoint>(Waypoint);

				if (WaypointItr)
				{
					if (WaypointItr->GetWaypointOrder() == CurrentWaypoint)
					{
						EnemyAIController->MoveToActor(WaypointItr, 10.f, false);
						CurrentWaypoint++;
						break;
					}
				}
			}
		}
	}
}

void AEnemyAI::Die()
{
	OnEnemyDeath.Broadcast(this);
	Destroy();
}

