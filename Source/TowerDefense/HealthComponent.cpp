// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthComponent.h"
#include "TowerDefenseGameModeBase.h"
#include "Kismet/GameplayStatics.h"
#include "EnemyAI.h"


// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	TowerGameMode = Cast<ATowerDefenseGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()->GetAuthGameMode()));
	// ...

}


// Called every frame
void UHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UHealthComponent::TakeDamage(int32 damage)
{
	CurrentHP -= damage;

	if (CurrentHP <= 0)
	{
		TowerGameMode->enemiesKilled++;
		CurrentHP = 0;
		enemyDead.Broadcast();
	}
}

void UHealthComponent::InitializeHealth(int32 healthMultiplier)
{
	MaxHP = MaxHP * healthMultiplier;
	CurrentHP = MaxHP;
}

float UHealthComponent::GetCurrentHP()
{
	return CurrentHP;
}

