// Fill out your copyright notice in the Description page of Project Settings.


#include "TowerGhost.h"
#include "BuildManager.h"
#include "TowerNode.h"
#include "TowerDefenseGameModeBase.h"
#include "TowerData.h"
#include "Tower.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "Components/ArrowComponent.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"

// Sets default values
ATowerGhost::ATowerGhost()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	TowerBody = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Tower Body"));
	TowerBarrel = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Tower Barrel"));
	Sphere = CreateDefaultSubobject<USphereComponent>(TEXT("Range Indicator"));

	TowerBody->SetupAttachment(RootComponent);
	TowerBarrel->SetupAttachment(RootComponent);
	Sphere->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ATowerGhost::BeginPlay()
{
	Super::BeginPlay();
	
	TowerBody->OnClicked.AddDynamic(this, &ATowerGhost::OnClicked);
	TowerBarrel->OnClicked.AddDynamic(this, &ATowerGhost::OnClicked);
	IsSpawn = true;
	GameMode = Cast<ATowerDefenseGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
	PlayerController = UGameplayStatics::GetPlayerController(this, 0);
}

// Called every frame
void ATowerGhost::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	GhostToMouse();

	if (PlayerController->WasInputKeyJustPressed(FKey("LeftMouseButton")))
	{
		this->Destroy();
	}
}

void ATowerGhost::SetRangePreview(FVector RangeScale)
{
	Sphere->SetWorldScale3D(FVector(RangeScale));
}

void ATowerGhost::GhostToMouse()
{
	if (IsSpawn)
	{
		FVector WorldLocation;
		FVector WorldDirection;
		float DistanceAboveGround = 200.0f;

		PlayerController->DeprojectMousePositionToWorld(WorldLocation, WorldDirection);

		FVector PlaneOrigin(0.0f, 0.0f, DistanceAboveGround);

		FVector ActorWorldLocation = FMath::LinePlaneIntersection(WorldLocation, WorldLocation + WorldDirection, PlaneOrigin, FVector::UpVector);
		SetActorLocation(ActorWorldLocation);
		UpdateGhostMaterial();
	}
}

void ATowerGhost::UpdateGhostMaterial()
{
	FHitResult Hit;
	TArray<TEnumAsByte<EObjectTypeQuery>> ObjectType;
	ObjectType.Add(UEngineTypes::ConvertToObjectType(ECC_WorldDynamic));
	PlayerController->GetHitResultUnderCursorForObjects(ObjectType, true, Hit);

	ATowerNode* node = Cast<ATowerNode>(Hit.GetActor());

	if (Hit.bBlockingHit)
	{
		if (node != nullptr)
		{
			//Arrow = node->Arrow;

			TowerBody->SetMaterial(0, GreenMaterial);
			TowerBarrel->SetMaterial(0, GreenMaterial);
			FVector Location = TowerBody->GetComponentLocation();
			TowerBarrel->GetComponentLocation();
			//node->Tower = GameMode->TowerData[DataIndex]->Tower;
			DrawDebugSphere(GetWorld(), Location, 50, 26, FColor(0, 200, 0), false, -1, 0, 8);
		}
		else
		{
			TowerBody->SetMaterial(0, RedMaterial);
			TowerBarrel->SetMaterial(0, RedMaterial);
			FVector Location = TowerBody->GetComponentLocation();
			TowerBarrel->GetComponentLocation();
			DrawDebugSphere(GetWorld(), Location, 50, 26, FColor(200, 0, 0), false, -1, 0, 8);
		}
	}
}

void ATowerGhost::SpawnTower()
{
	if (IsSpawn)
	{
		UWorld* World = GetWorld();

		ATower* tower = World->SpawnActor<ATower>(GameMode->TowerData[DataIndex]->Tower, Arrow->GetComponentLocation(), Arrow->GetComponentRotation());
		this->Destroy();
	}
}

void ATowerGhost::OnClicked(UPrimitiveComponent* TouchedComponent, FKey ButtonPressed)
{
}
