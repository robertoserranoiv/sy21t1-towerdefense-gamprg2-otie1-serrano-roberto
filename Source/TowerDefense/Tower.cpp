// Fill out your copyright notice in the Description page of Project Settings.


#include "Tower.h"
#include "EnemyAI.h"
#include "Projectile.h"
#include "Components/StaticMeshComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/SphereComponent.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
ATower::ATower()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	TowerBody = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Tower Body"));
	TowerBarrel = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Tower Barrel"));
	Arrow = CreateDefaultSubobject<UArrowComponent>(TEXT("Muzzle"));
	SphereCollider = CreateDefaultSubobject<USphereComponent>(TEXT("Range"));

	TowerBody->SetupAttachment(RootComponent);
	TowerBarrel->SetupAttachment(RootComponent);
	Arrow->SetupAttachment(TowerBarrel);
	SphereCollider->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ATower::BeginPlay()
{
	Super::BeginPlay();

	SphereCollider->OnComponentBeginOverlap.AddDynamic(this, &ATower::OnBeginOverlap);
	SphereCollider->OnComponentEndOverlap.AddDynamic(this, &ATower::OnEndOverlap);
}

void ATower::OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AEnemyAI* enemy = Cast<AEnemyAI>(OtherActor))
	{
		Targets.Add(enemy);
		CurrentTarget = Targets[0];
	}
}

void ATower::OnEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (AEnemyAI* enemy = Cast<AEnemyAI>(OtherActor))
	{
		Targets.Remove(enemy);
		if (Targets.Num() > 0)
		{
			CurrentTarget = Targets[0];
		}
		else
		{
			CurrentTarget = nullptr;
		}
	}
}

void ATower::ShootTarget()
{
	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Blue, "Firing");
	if (CurrentTarget)
	{
		if (Projectile)
		{
			AProjectile* projectile = GetWorld()->SpawnActor<AProjectile>(Projectile, Arrow->GetComponentTransform());

			if (projectile)
			{
				projectile->SetDamage(TowerDamage);
			}
		}
	}
}

void ATower::Aim()
{
	if (CurrentTarget)
	{
		FVector targetLocation = CurrentTarget->GetActorLocation() + TowerOffset;
		FVector towerLocation = TowerBarrel->GetComponentLocation();
		FVector aimDirection = targetLocation - towerLocation;
		FRotator towerRotation = UKismetMathLibrary::Conv_VectorToRotator(aimDirection);

		TowerBarrel->SetWorldRotation(towerRotation);
		Arrow->SetWorldRotation(towerRotation);
	}
}

// Called every frame
void ATower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (CurrentTarget != nullptr)
	{
		CurrentTarget = Targets[0];

		if (IsActive)
		{
			Aim();

			if (FireTime <= 0)
			{
				ShootTarget();
				FireTime = 1.0f / FireRate;
			}
			else
			{
				FireTime -= DeltaTime;
			}
		}
	}
}

