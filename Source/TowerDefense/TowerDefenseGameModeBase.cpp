// Copyright Epic Games, Inc. All Rights Reserved.

#include "TowerDefenseGameModeBase.h"
#include "EnemyAI.h"
#include "PlayerCore.h"
#include "Spawner.h"
#include "WaveData.h"
#include "TowerData.h"
#include "Kismet/GameplayStatics.h"
#include "TimerManager.h"
	
void ATowerDefenseGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	//Core
	CoreCurrentHp = CoreMaxHp;

	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AActor::StaticClass(), CoreActors);
	

	for (AActor* Actors : CoreActors)
	{
		APlayerCore* coreActor = Cast<APlayerCore>(Actors);
			
		if (coreActor)
		{
			coreActor->CoreHit.AddDynamic(this, &ATowerDefenseGameModeBase::CoreOnHit);
		}
	}

	//Spawners
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASpawner::StaticClass(), Spawners);

	for (AActor* spawners : Spawners)
	{
		ASpawner* spawner = Cast<ASpawner>(spawners);

		if (spawner)
		{
			spawner->endWave.AddDynamic(this, &ATowerDefenseGameModeBase::OnWaveEnded);
		}
	}
	
	//EnemyAI->OnEnemyDeath.AddDynamic(this, &ATowerDefenseGameModeBase::OnEnemyDied);
	//EnemyAI->OnEnemyKilledByProjectile.AddDynamic(this, &ATowerDefenseGameModeBase::OnEnemyKilledByProjectile);
}

void ATowerDefenseGameModeBase::OnWaveEnded(ASpawner* spawner)
{
	if (enemiesKilled >= spawner->WaveData[spawner->GetCurrentWave()]->NumSpawns * (Spawners.Num()))
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Blue, "Enemies Clear");
		totalEnemiesKilled += enemiesKilled;
		enemiesKilled = 0;
		
		//PlayerGold += WaveData[currentWave]->ClearReward;

		for (int32 i = 0; i < Spawners.Num(); i++)
		{
			ASpawner* spawner = Cast<ASpawner>(Spawners[i]);
			spawner->StartNextWave();
			//PlayerGold += WaveData[currentWave]->ClearReward;
		}
		waveEndDelay.Invalidate();
	}

	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Blue, "Enemies still not clear");
		FTimerDelegate TimerDelegate = FTimerDelegate::CreateUObject(this, &ATowerDefenseGameModeBase::OnWaveEnded, spawner);
		GetWorldTimerManager().SetTimer(waveEndDelay, TimerDelegate, 2.0f, false);
	}
}

void ATowerDefenseGameModeBase::CoreTakeDamage(int damage)
{
	if (CoreCurrentHp > 0)
	{
		CoreCurrentHp -= damage;
	}

}

void ATowerDefenseGameModeBase::CoreOnHit(class APlayerCore* coreActor)
{
	CoreTakeDamage(2);
	enemiesKilled++;

	if (CoreCurrentHp <= 0)
	{
		CoreCurrentHp = 0;
		CoreDestroyed();
	}

	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, TEXT("Core Damaged"));
}

void ATowerDefenseGameModeBase::CoreDestroyed()
{
	FTimerHandle TimerHandle;
	FTimerDelegate TimerDelegate = FTimerDelegate::CreateUObject(this, &ATowerDefenseGameModeBase::RestartGame);
	GetWorldTimerManager().SetTimer(TimerHandle, TimerDelegate, 2.0f, false);
}

void ATowerDefenseGameModeBase::RestartGame()
{
	UGameplayStatics::OpenLevel(this, FName(*GetWorld()->GetName(), false));
}

int32 ATowerDefenseGameModeBase::AddGold(AEnemyAI* enemy)
{
	return PlayerGold += enemy->GoldReward;
}

int32 ATowerDefenseGameModeBase::AddGoldWave(int32 amount)
{
	return PlayerGold += amount;
}

void ATowerDefenseGameModeBase::SetGoldScale(int32 GoldMultiplier)
{
	PlayerGold = PlayerGold * GoldMultiplier;
}

bool ATowerDefenseGameModeBase::IsPlayerGoldEnough(float cost)
{
	if (PlayerGold >= cost)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void ATowerDefenseGameModeBase::DeductGold(float cost)
{
	PlayerGold -= cost;
}